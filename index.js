/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printUserInfo(){
	let fullName = prompt("What is your Name?");
	let oldAge = prompt("How old are you?");
	let liveIn = prompt("Where do you live?");
	alert("Thank You for your input!");

	console.log("Hello, " + fullName + "!");
	console.log("You are " + oldAge + " years old.");
	console.log("You live in " + liveIn);
}

	printUserInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands(){
	let bands = ["IV of Spades", "Zack Tabudlo", "Arthur Nery", "Oasis", "LANY"];

	for(let fav = 0; fav < 5; fav++){
		let numbering = fav + 1;
		console.log(numbering + ". " + bands[fav])
	}
}

favoriteBands();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies(){
	let bands = ["Breaking Bad", "Forrest Gump", "The Iron Giant", "The Prince of Egypt", "The Pianist"];
	let moviesScore = ["96%", "71%", "96%", "79%", "95%"];

	for(let fav = 0; fav < 5; fav++){
		let numbering = fav + 1;
		console.log(numbering + ". " + bands[fav]);
		console.log("Rotten Tomatoes Rating: " + moviesScore[fav]);
	}
}

favoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

}

printUsers();



